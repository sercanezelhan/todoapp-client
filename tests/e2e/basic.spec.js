describe('your Vue app', () => {
  beforeAll(async () => {
    await page.goto('http://13.93.44.232:8080/');
  });

  it('can be tested with jest and puppeteer', async () => {
    await expect(page).toMatchElement('.todoButton','Create')
    await expect(page).toMatchElement('.todoInput','Create Todo')
  });
});
