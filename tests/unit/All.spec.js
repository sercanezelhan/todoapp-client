import { mount } from '@vue/test-utils';
import All from '@/components/All.vue'
import { expect } from 'chai';


describe('All.vue', () => {
     
  it('All todos should appear.', () => {
    const wrapper = mount(All);
    wrapper.setData({
             allTodoList:[{  content:"test0",
             id:"1234q"},{  content:"test1",
             id:"1234w"}]
        });
    expect(wrapper.vm.allTodoList.length).to.equal(2)
  })

  it('One todo will be added when the button is clicked.', () => {
    const wrapper = mount(All);
    var button=wrapper.find('.todoButton')
    var input=wrapper.find('.todoInput')
    input.setValue("test")
    button.trigger('click') 
    })
})