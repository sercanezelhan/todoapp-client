const puppeteer =require('puppeteer')
var {setDefaultTimeout} = require('@cucumber/cucumber');
const { Then, Before, When,  Given,After} = require('@cucumber/cucumber');
const { expect } = require('chai')
setDefaultTimeout(60 * 1000);
let browser
let page

Before(async function(){
    browser= await puppeteer.launch({ headless: true });
    page= await browser.newPage()
    await page.goto('http://13.93.44.232:8080/',{waitUntil: 'load', timeout: 100000})
})
After(async function(){
   await browser.close()
})
Given('Empty list',()=>{ // nothing
})
When('Write {string} to {string} and click to {string}',async function(todoText,inputElement,clickElement){
    await page.type(inputElement,todoText)
    await page.click(clickElement)
    await new Promise(r => setTimeout(r, 3000));
})
Then('Should see {string} item in allTodoList',async function(todoText){
    const allTodos = await page.evaluate(() => Array.from(document.getElementsByClassName('todoContent'), e => e.innerText));
    var firstAddedTodo=allTodos[0].split("\n")
    expect(firstAddedTodo[0]).to.equal(todoText)
})
