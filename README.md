# ToDo Application

Bu uylama; todo eklemek için golang ile geliştirilmiş vue ile bir arayüze sahip olarak geliştirilmiştir.
Uygulamaya erişim için : http://13.93.44.232:8080/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
